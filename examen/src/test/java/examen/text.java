package examen;

import static org.junit.Assert.*;

import org.easymock.bytebuddy.implementation.bind.annotation.IgnoreForBinding.Verifier;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

public class text {

	Exam1 ex;

	@Before
	public void antes() {
		ex = new Exam1();
	}

	@Parameters
	public void parametros() {

	}

	@Test
	public void testGetRestZero() {
		
		
		assertEquals(true, ex.getRestZero(2, 2));

	}
	
	@Test
	public void testOneHalf() {
		
		assertEquals(2, ex.oneHalf(4));
	

	}

}
